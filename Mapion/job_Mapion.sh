#!/bin/bash
#SBATCH -o log_mapion.out
#SBATCH -J Mapion
#SBATCH -A ADD_ACC_HERE
#SBATCH -n 1 KEEP ONE CORE
#SBATCH --mem=14000 HIGH MEM IS NEEDED
#SBATCH --time=160:00:00 CHANGE TIME

#This is the main slurm-script to be submitted. The job.sh will be used by the Mapion code.

#uncoment one of these options:
#To run directly:
#./Mapion.py

#Or through python:
#PATH_TO_YOUR_PYTHON3/python3 -u Mapion.py

