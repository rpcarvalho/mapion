# Mapion

This is an algorithm to study ion/insertion processes. It works by successively inserting a specified atom/ion in a given structure. Educated guesses are used to place the ion inside the structure by analyzing its potential energy surface (obtained through VASP/DFT). From this, the structure is relaxed before the process can be repeated for the next ion-insertion step.

A more detailed explanation can be find in the paper [REF].

This code was originally prepared to work with VASP and the SLURM job scheduler. In case of using VASP/SLURMS, a 'potcars' folder containing VASP POTCAR files for all the strucutre's elements and the SLURM scripts 'job_Mapion.sh' and 'job.sh' are required, both provided together with this code. In addition, the INCAR1 and INCAR2 files also must be present. The former control the parameters for the initial calculation made with the initial/pristine structure and the latter for calculations made for the ion-inserted phases. Since they are simple VASP input files, you can modify them as you like. However, the potential LVTOT related tags are mandatory for the code to work. Finally, the POSCAR of the investigated structure must also be present.

### Dependencies:
This package is based on common python libraries and also depend on external softwares.
* python3
* numpy
* SLURM*
* VASP*

Additionally, the following files must be present in the same work directory as this code:
* INCAR1
* INCAR2
* POSCAR (of the initial structure)
* potcars/{relevant POTCAR_element files}
* job.sh
* job_Mapion.sh

*feel free to modify this code to work with different quantum mechanics codes and/or job schedulers.

----
Please, feel free to contact me at rodrigo.carvalho.al@gmail.com or rodrigo.carvalho@physics.uu.se.