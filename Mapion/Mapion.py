#!/bin/python3
# change the correct python env
"""
This is an algorithm to study ion/insertion process. It works by successively inserting
a specified atom(ion) in a given structure. The potential energy surface of
the structure (obtained through VASP/DFT) is used to search for the most suitable
place to insert the ion. From this, the structure is relaxed before the
process can be repeated for the next ion-insertion process.

A more detailed explanation can be find in the paper [REF].

This code was originally prepared to work with VASP and the SLURM job scheduler.
In case of using VASP/SLURMS, a 'potcars' folder containing VASP POTCAR files for
all the strucutre's elements and the SLURM scripts 'job_LocLi.sh' and 'job.sh'
are required, both provided together with this code.

In addition, the INCAR1 and INCAR2 files also must be present. The former control
the parameters for the initial calculation made with the initial/pristine structure 
and the latter for calculations made for the ion-inserted phases. Since they are
simple VASP input files, you can modify them as you like. However, the potential
LVTOT related tags are mandatory for the code to work. 

Created by © Rodrigo Carvalho 2020
Maintained by © Rodrigo Carvalho
"""

import numpy as np
import os
import time
import gc

# DEFINE HERE SOME GENERAL PARAMETERS
number_of_steps = 8  # NUMBER OF ELEMENTS/IONS TO ADD
nkpts_1 = 4  # KMESH FOR THE FIRST IONIC OPTMIZATION
nkpts_2 = 6  # KMESH FOR THE SECOND IONIC OPTMIZATION
encut1 = 500  # ENCUT FOR THE FIRST IONIC OPTMIZATION
encut2 = 600  # ENCUT FOR THE SECOND IONIC OPTMIZATION
check_interval = 60  # INTERVAL TO CHECK THE SLURM/VASP RESULTS
element = "Li"  # ELEMENT TO BE ADDED
kind = "cation" # "cation" / "anion"


######################################
#### DO NOT TOUCH ANYTHING BELLOW ####
######################################


# initializing dirs
main_dir = os.getcwd() + "/"
pot_dir = main_dir + "potcars/"
calcs_dir = main_dir + "calcs/"

# check the existence of previous calculation
if os.path.exists(calcs_dir) == True:
    print("Calcs directory is present, check your results before restarting.")
    print("EXIT")
    exit()
else:
    os.mkdir("calcs")


# FUNCTIONS #############################################
# JUMP THIS PART ########################################
#########################################################


def check(interval):
    """Check in 'interval' seconds if the calculation has finished"""
    key = False
    while key == False:
        time.sleep(interval)
        lis = os.listdir()
        for i in lis:
            if i == "DONE":
                key = True
                return 1
    return 0


# functions
def potcar(atoms):
    """Write POTCARs"""
    txt = ""
    for i in atoms:
        txt = txt + " " + pot_dir + "POTCAR_" + str(i)
    os.system("cat " + txt + " > POTCAR")


def read_poscar(file):
    """Read POSCAR and return atoms,Nspecies,Natoms,lat,coord"""
    # read lattice
    file_chg = open(file, mode="r")
    x = file_chg.read().splitlines()
    end = file_chg.tell()

    # read parameters
    atoms = x[5].split()
    Nspecies = x[6].split()
    Natoms = [int(i) for i in Nspecies]
    Natoms = sum(Natoms)
    n = 3
    lat = []
    vol = x[1]
    for n in (2, 3, 4):
        lat.append(x[n].split())
    lat = np.array(lat, dtype=np.float)

    coord = []
    nn = 9 if list(x[7])[0] == "S" else 8
    for n in range(nn, nn + Natoms):
        coord.append(x[n].split())
    coord = np.array(coord)

    return atoms, Nspecies, Natoms, lat, coord, vol


def kpts(nkpts):
    """Write KPOINTS file"""
    with open("KPOINTS", "w") as kpt:
        kpt.write(
            "EKP \n0 \nGamma \n"
            + str(nkpts)
            + "  "
            + str(nkpts)
            + "  "
            + str(nkpts)
            + "\n"
        )


def new_poscar(name,element,newp,atoms,Nspecies,Natoms,lat,coord,vol,kind = 'Direct'):
    #add one new atom to list
    if element in atoms:
        atms = atoms
        posit = atms.index(element)
        Nspc_new = np.array(Nspecies,dtype=np.int)
        Nspc_new[posit] = Nspc_new[posit] + 1
    else:
        atms = [element]+atoms
        Nspc = [0] + Nspecies
        posit = atms.index(element)
        Nspc_new = np.array(Nspc,dtype=np.int)
        Nspc_new[posit] = Nspc_new[posit] + 1
        os.system('mv POSCAR POSCAR_back')
    with open('POSCAR',mode='w') as f:
        f.write(str(name)+'\n')
        f.write(vol+'\n')
        f.write('    '+' '.join(str(tx) for tx in lat[0])+'\n')
        f.write('    '+' '.join(str(tx) for tx in lat[1])+'\n')
        f.write('    '+' '.join(str(tx) for tx in lat[2])+'\n')
        f.write(' '+' '.join(str(tx) for tx in atms)+'\n')
        f.write(' '+' '.join(str(tx) for tx in Nspc_new)+'\n')
        f.write(kind+'\n')
        #write new position
        f.write(' '.join(str(tx) for tx in newp)+'\n')
        for i in range(len(coord)):
            f.write(' '.join(str(tx) for tx in coord[i])+'\n')


def pc_position(file, kind=kind, coord="direct"):
    """Use LOCPOT or CHGCAR as input

    kind can be 'cation' or 'anion'
    coord can be 'direct' or 'cartesian'

    Return: Position of minimun/maximun in direct coordenates"""
    # read lattice
    file_chg = open(file, mode="r")
    x = file_chg.read().splitlines()
    end = file_chg.tell()

    # read parameters
    atoms = x[5].split()
    Nspecies = x[6].split()
    Natoms = [int(i) for i in Nspecies]
    Natoms = sum(Natoms)
    dimm = x[9 + Natoms].split()
    dimm = [int(i) for i in dimm]
    dimm3 = int(dimm[0]) * int(dimm[1]) * int(dimm[2])
    n = 3
    lat = []
    for n in (2, 3, 4):
        lat.append(x[n].split())
    lat = np.array(lat, dtype=np.float)

    # read potential/charge
    data = []
    for n in np.arange(
        10 + Natoms, int(10 + Natoms + dimm3 / 5)
    ):  # np.arange(10+Natoms,10+Natoms+10): #
        for i in (0, 1, 2, 3, 4):
            data.append(x[n].split()[i])
    data = [float(i) for i in data]
    data = np.array(data)
    data2 = data.reshape((dimm[0], dimm[1], dimm[2]))

    # get the minimum pot
    best = data2[0, 0, 0]
    if kind == "cation":
        for k in range(dimm[2]):
            for j in range(dimm[1]):
                for i in range(dimm[0]):
                    trial = data2[i, j, k]
                    if trial >= best:
                        best = trial
                        c1 = i
                        c2 = j
                        c3 = k
    else:
        for k in range(dimm[2]):
            for j in range(dimm[1]):
                for i in range(dimm[0]):
                    trial = data2[i, j, k]
                    if trial <= best:
                        best = trial
                        c1 = i
                        c2 = j
                        c3 = k

    if coord == "direct":
        x1 = np.linspace(0, 1, dimm[2])[c3]
        y1 = np.linspace(0, 1, dimm[1])[c2]
        z1 = np.linspace(0, 1, dimm[0])[c1]
        return np.array([x1, y1, z1])

    if coord == "cartesian":
        x1 = np.linspace(0, 1, dimm[2])[c3]
        y1 = np.linspace(0, 1, dimm[1])[c2]
        z1 = np.linspace(0, 1, dimm[0])[c1]
        xx = np.array([x1, y1, z1])
        return np.matmul(lat, xx)


########################################################
########################################################
## END OF FUNCTIONS ####################################


#### PROCESS
n = number_of_steps
os.chdir(calcs_dir)
energy = []
for i in range(n + 1):
    gc.collect()
    os.mkdir("add" + str(i))
    os.chdir("add" + str(i))
    # first structure = no add
    if i == 0:
        os.system("cp " + main_dir + "POSCAR .")
        os.system("cp " + main_dir + "INCAR1 INCAR")
        os.system("cp " + main_dir + "job.sh .")
        atoms, Nspecies, Natoms, lat, coord, vol = read_poscar("POSCAR")
        potcar(atoms)

        kpts(nkpts_1)
        # FIRST CALCULATION
        os.system("sed -i 's/ENCUT.*/ENCUT = " + str(encut1) + "/' INCAR")
        os.system("sed -i 's/PREC.*/PREC = Normal/' INCAR")
        # submit job
        os.system("sbatch job.sh")
        # check calculation
        tt = check(check_interval)

        kpts(nkpts_2)
        os.system("rm DONE")
        # SECOND CALCULATION
        os.system("sed -i 's/ENCUT.*/ENCUT = " + str(encut2) + "/' INCAR")
        os.system("sed -i 's/PREC.*/PREC = Accurate/' INCAR")
        os.system("cp CONTCAR POSCAR")
        # submit job
        os.system("sbatch job.sh")
        # check calculation
        tt = check(check_interval)
        os.system("rm -f WAVECAR")


        # read LOCPOT and get new position
        newp = pc_position("LOCPOT")
        # get energy
        energy.append(
            float(os.popen("grep TOTEN OUTCAR | tail -1 | awk '{print $5}'").read())
        )

    else:
        os.system("cp " + calcs_dir + "add" + str(i - 1) + "/CONTCAR POSCAR")
        os.system("cp " + main_dir + "INCAR2 INCAR")
        os.system("cp " + main_dir + "job.sh .")
        atoms, Nspecies, Natoms, lat, coord, vol = read_poscar("POSCAR")
        new_poscar(
            "add" + str(i), element, newp, atoms, Nspecies, Natoms, lat, coord, vol
        )
        atoms, Nspecies, Natoms, lat, coord, vol = read_poscar("POSCAR")
        potcar(atoms)
        os.system("cp POSCAR POSCAR_new")

        kpts(nkpts_1)
        # FIRST CALCULATION
        os.system("sed -i 's/ENCUT.*/ENCUT = " + str(encut1) + "/' INCAR")
        os.system("sed -i 's/PREC.*/PREC = Accurate/' INCAR")
        # submit job
        os.system("sbatch job.sh")
        # check calculation
        tt = check(check_interval)

        kpts(nkpts_2)
        os.system("rm DONE")
        # SECOND CALCULATION
        os.system("sed -i 's/ENCUT.*/ENCUT = " + str(encut2) + "/' INCAR")
        os.system("sed -i 's/PREC.*/PREC = Accurate/' INCAR")
        os.system("cp CONTCAR POSCAR")
        # submit job
        os.system("sbatch job.sh")
        # check calculation
        tt = check(check_interval)
        os.system("rm -f WAVECAR")


        # read LOCPOT and get new position
        newp = pc_position("LOCPOT")
        # get energy
        energy.append(
            float(os.popen("grep TOTEN OUTCAR | tail -1 | awk '{print $5}'").read())
        )

    os.chdir("..")

os.chdir("..")


# writing results
os.chdir(main_dir)

with open("energy", "w") as f:
    for i in range(len(energy)):
        f.write(str(i) + "   " + str(energy[i]) + "\n")

## END
